package com.example.prueba;
/**
 * @author Pablo N�jera (najera@lcc.uma.es)
 * @author Paco Mart�n (erespia2@gmail.com)
 * 
 */
import com.dephisit.sensorinfoforandroid.SensorInfo;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	public static Activity activity;
	private SensorInfo si;
	//private EditText responseT;
	private TextView responseT,prevResponsesT;
	private String lastMessage="";
	private String previousMessages="";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		activity = this;
		setContentView(R.layout.activity_main);
		//responseT = (EditText) this.findViewById(R.id.response);
		responseT = (TextView) this.findViewById(R.id.response);
		prevResponsesT = (TextView) this.findViewById(R.id.previous_responses);
		
		// Ejemplo de utilizaci�n de la API de Bluetooth que se creara. Solo estas tres lineas y la clase 'CallbackSensor'
		// La clase definida en esta app, y que se deber� definir en cada app que quiera usar la API
		// 'CallbackSensor', es la que tiene la funcion que llamar� la API cuando quiera informar de
		// un nuevo valor del sensor relevante.
		si = new SensorInfo();
		si.setParams(new CallbackSensor(this,this), this);
		si.execute();
		si.notifyAccelerometers(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}	

    @Override
    protected void onResume() {
    	super.onResume();
    	si.onResume();
    }

    @Override
    protected void onPause() {
    	super.onPause();
    	si.onPause();
    }

    @Override
    protected void onDestroy() {
    	super.onDestroy();
    	si.onDestroy();
    }
    
    protected void messageReceived(String message){
    	if (message!=null){
    		previousMessages= lastMessage+"\n"+previousMessages;
    		lastMessage = message;
    		responseT.setText(lastMessage);
    		prevResponsesT.setText(previousMessages);
    	}    	
    }

}
