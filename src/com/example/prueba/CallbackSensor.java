package com.example.prueba;
/**
 * @author Pablo N�jera (najera@lcc.uma.es)
 * @author Paco Mart�n (erespia2@gmail.com)
 * 
 */
import com.dephisit.sensorinfoforandroid.IStandardTaskListener;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

// Clase que se debe definir en cada app que quiera usar la API del Bluetooth, para
// que esta le notifique los cambios relevantes. Por ello extiende de una interfaz definida
// en la propia API para que esta sepa a quien debe informar, mediante herencia
public class CallbackSensor implements IStandardTaskListener {
	private final static String TAG = "DEPH App Example";
	private Context context;
	private MainActivity mainActivity;
	
	// Inicializacion del contexto en el constructor, por su hace falta
    public CallbackSensor(Context context, MainActivity mainActivity) {
    	this.context = context;
    	this.mainActivity = mainActivity;
    }
    
    // Funcion que har� de callback para que la API sepa mediante donde informar a la app, o sea, mediante esta funcion.
    @Override
    public void taskComplete(Object result) {
    	Log.v(TAG, "Received data from our API!");
    	if (result != null){
    		// Convertir el Object 'result' al tipo de dato que se define como que nos pasan (JSONObject, una entidad declarada especifica para trabajar con ella, u cualquier otra cosa)
    		Log.v(TAG,result.toString());
    		mainActivity.messageReceived((String) result);
		} else {
			Log.v(TAG,"Message from SensorInfo API was empty!");
		}
    }
    
}
